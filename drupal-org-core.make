; A separate drupal-org-core.make file makes it so we can apply core patches
; if we need to.

api = 2
core = 7.x
projects[drupal][type] = core
projects[drupal][version] = 7.53

; CORE PATCHES

; Raise minimum PHP version to work around core requirements check bug.
; Keep eyeballs posted on https://drupal.org/node/1724130.
projects[drupal][patch][1724012] = https://drupal.org/files/drupal-increase-php-version-1724012_0.patch
; Adding partial word support to core. It might lead to performance issues, but
; better a slow search than a strict one.
projects[drupal][patch][498752] = https://www.drupal.org/files/search-partial_words_hack-498752-19.patch
