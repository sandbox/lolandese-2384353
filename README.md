## Sparkle, an updated and modified Spark fork ##

### Download ###

If your sites are in the www folder:

- cd www
- git clone --depth=1 --branch 7.x-1.x http://git.drupal.org/sandbox/lolandese/2384353.git sparkle
- rm -rf !$/.git
- cd sparkle

More info at:
http://stackoverflow.com/a/11498124/523688


### Usage ###

With Drush 7.0 to build the profile, run:

drush make build-sparkle.make ~/www/yoursite --prepare-install --no-recursion --overwrite --strict=0

This will assemble a copy of Drupal core with all needed contrib (or the latest
copy of the Sparkle distro files) from Git, along with all of its dependencies.

- Point your example.dev domain to the 'yoursite' folder.
- Create an empty database and DB user for the new site.
- Visit example.dev and install your site as usual.
- Check for errors/warnings in
  - admin/reports/status
  - admin/reports/dblog
- Additionally check for available updates for all projects:
  drush ups --check-disabled
  Feel free to open a 'Task' issue if so.


### How to reproduce the exact same build later ###

Dev versions are a moving target. If you use the build command above later, you
might get a 'Sparkle' profile folder that has modified files (not the same as
this folder). Although the build itself will be according to what is defined in
the '.make' files here, it still might be a cause of confusio. To avoid that
read the comment lines at the bottom of the *build-sparkle.make* file.


### The distro files ###

There are two other .make files, which are called from the "build" one:

#### drupal-org-core.make ####
This file just contains the definition for how to package Drupal core,
including core patches.

#### drupal-org.make ####
This file is where the bulk of the work happens; contributed modules/themes,
external libraries, non-core patches, etc. are all handled here.
Be aware that all contrib dev versions can change at any time, causing errors.
To avoid that stable versions should be used or a revision number should be
given.

Furthermore:

#### sparkle.info ####
Telling what core and contrib should be initially enabled.

#### sparkle.install ####
Setting up the rest, e.g.:
- Variable values.
- Text formats.
- Blocks.
- Entity (e.g. node) types (e.g. article, page).
- Taxonomy vocabularies.
- Fields.
- Roles.
- Permissions.
- Menus.
- Theme settings.


### More info ###

See https://drupal.org/developing/distributions.
