api = 2
core = 7.x
; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make
; Uncomment the line below once this turns into a full project:
includes[] = drupal-org.make

; Download the Sparkle install profile and recursively build all its dependencies:
projects[sparkle][type] = profile
projects[sparkle][download][type] = git
projects[sparkle][download][branch] = 7.x-1.x
projects[sparkle][download][url] = http://git.drupal.org/sandbox/lolandese/2384353.git

; Dev versions are a moving target. To reproduce exactly the same build later
; also in the 'profile' folder (only to avoid confusion) find the revision at
; https://www.drupal.org/node/2384353/commits and go to the date on or right
; after the 'Date Modified' of this 'Sparkle folder' as indicated on your local
; machine or server. Then modify and uncomment the line below:

;projects[sparkle][download][revision] = 264942d

