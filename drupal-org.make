; See https://drupal.org/node/159730 for more details.

api = 2
core = 7.x


; Contributed themes.

projects[responsive_bartik][type] = theme
projects[responsive_bartik][version] = 1.0
projects[responsive_bartik][subdir] = contrib

projects[gratis][type] = theme
projects[gratis][version] = 2.10
projects[gratis][subdir] = contrib

projects[ember][type] = theme
projects[ember][version] = 2.0-alpha4
projects[ember][subdir] = contrib


; Contributed modules, feeds related.

projects[feeds][type] = module
projects[feeds][subdir] = contrib
projects[feeds][version] = 2.0-beta3

projects[feeds_ex][type] = module
projects[feeds_ex][subdir] = contrib
projects[feeds_ex][version] = 1.0-beta2

projects[feeds_tamper][type] = module
projects[feeds_tamper][subdir] = contrib
projects[feeds_tamper][version] = 1.1


; Contributed modules, field related.

projects[date][type] = module
projects[date][subdir] = contrib
projects[date][version] = 2.9

projects[geophp][type] = module
projects[geophp][subdir] = contrib
projects[geophp][version] = 1.7

projects[geofield][type] = module
projects[geofield][subdir] = contrib
projects[geofield][version] = 2.3

projects[token_field][type] = module
projects[token_field][subdir] = contrib
projects[token_field][version] = 1.x-dev
projects[token_field][download][revision] = cef5ee3

projects[url][type] = module
projects[url][subdir] = contrib
projects[url][version] = 1.0


; Contributed modules, Views related.

projects[admin_views][type] = module
projects[admin_views][subdir] = contrib
projects[admin_views][version] = 1.6

projects[better_exposed_filters][type] = module
projects[better_exposed_filters][subdir] = contrib
projects[better_exposed_filters][version] = 3.2

projects[ctools][type] = module
projects[ctools][version] = 1.12
projects[ctools][subdir] = contrib

projects[views][type] = module
projects[views][version] = 3.14
projects[views][subdir] = contrib

projects[admin_views][type] = module
projects[admin_views][version] = 1.6
projects[admin_views][subdir] = contrib

projects[draggableviews][type] = module
projects[draggableviews][version] = 2.1
projects[draggableviews][subdir] = contrib

projects[views_bulk_operations][type] = module
projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = 3.3

projects[viewfield][type] = module
projects[viewfield][subdir] = contrib
projects[viewfield][version] = 2.0


; Contributed modules, Flickr related.

projects[flickr][type] = module
projects[flickr][subdir] = contrib
projects[flickr][version] = 1.8

projects[autofloat][type] = module
projects[autofloat][subdir] = contrib
projects[autofloat][version] = 2.1

projects[field_html_trim][type] = module
projects[field_html_trim][subdir] = contrib
projects[field_html_trim][version] = 1.4

projects[field_formatter_settings][type] = module
projects[field_formatter_settings][subdir] = contrib
projects[field_formatter_settings][version] = 1.1

projects[style_settings][type] = module
projects[style_settings][subdir] = contrib
projects[style_settings][version] = 1.x-dev

projects[colorbox][type] = module
projects[colorbox][subdir] = contrib
projects[colorbox][version] = 2.12


; Contributed modules, others.

projects[backup_migrate][type] = module
projects[backup_migrate][subdir] = contrib
projects[backup_migrate][version] = 3.1

projects[breakpoints][type] = module
projects[breakpoints][version] = 1.4
projects[breakpoints][subdir] = contrib

projects[context][type] = module
projects[context][subdir] = contrib
projects[context][version] = 3.7

projects[ckeditor][type] = module
projects[ckeditor][version] = 1.17
projects[ckeditor][subdir] = contrib

projects[ds][type] = module
projects[ds][version] = 2.14
projects[ds][subdir] = contrib

projects[entity][type] = module
projects[entity][version] = 1.8
projects[entity][subdir] = contrib

projects[entityreference][type] = module
projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.2

projects[field_group][type] = module
projects[field_group][subdir] = contrib
projects[field_group][version] = 1.5

projects[file_entity][type] = module
projects[file_entity][subdir] = contrib
projects[file_entity][version] = 2.0-beta3

projects[flag][type] = module
projects[flag][subdir] = contrib
projects[flag][version] = 3.9

projects[globalredirect][type] = module
projects[globalredirect][subdir] = contrib
projects[globalredirect][version] = 1.x-dev
projects[globalredirect][download][revision] = e7debe9

projects[honeypot][type] = module
projects[honeypot][subdir] = contrib
projects[honeypot][version] = 1.22

projects[libraries][type] = module
projects[libraries][version] = 2.3
projects[libraries][subdir] = contrib

projects[navbar][type] = module
projects[navbar][subdir] = contrib
projects[navbar][version] = 1.7

projects[pathauto][type] = module
projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.3

projects[r4032login][type] = module
projects[r4032login][subdir] = contrib
projects[r4032login][version] = 1.8

projects[reroute_email][type] = module
projects[reroute_email][subdir] = contrib
projects[reroute_email][version] = 1.2

projects[rules][type] = module
projects[rules][subdir] = contrib
projects[rules][version] = 2.9

projects[search404][type] = module
projects[search404][subdir] = contrib
projects[search404][version] = 1.4

projects[stringoverrides][type] = module
projects[stringoverrides][subdir] = contrib
projects[stringoverrides][version] = 1.x-dev
projects[stringoverrides][download][revision] = 4229a48

projects[token][type] = module
projects[token][subdir] = contrib
projects[token][version] = 1.6

projects[view_unpublished][type] = module
projects[view_unpublished][subdir] = contrib
projects[view_unpublished][version] = 1.2

projects[simplified_menu_admin][type] = module
projects[simplified_menu_admin][version] = 1.0
projects[simplified_menu_admin][subdir] = contrib

; Libraries.
; NOTE: These need to be listed in https://drupal.org/packaging-whitelist.
libraries[underscore][download][type] = get
libraries[underscore][download][url] = https://github.com/jashkenas/underscore/archive/1.8.3.zip

libraries[backbone][download][type] = get
libraries[backbone][download][url] = https://github.com/jashkenas/backbone/archive/1.3.3.zip

libraries[modernizr][download][type] = get
libraries[modernizr][download][url] = https://github.com/Modernizr/Modernizr/archive/v2.8.3.tar.gz

libraries[geophp][download][type] = get
libraries[geophp][download][url] = https://github.com/downloads/phayes/geoPHP/geoPHP.tar.gz

libraries[colorbox][directory_name] = colorbox
libraries[colorbox][type] = library
libraries[colorbox][destination] = libraries
libraries[colorbox][download][type] = get
libraries[colorbox][download][url] = https://github.com/jackmoore/colorbox/archive/1.x.zip
